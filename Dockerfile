FROM ubuntu:18.04

RUN apt-get update; \
apt-get install -y python;\
apt-get clean

COPY www /var/www/html/
WORKDIR /var/www/html/

CMD ["/usr/bin/python","-m","SimpleHTTPServer"]

EXPOSE 8000

